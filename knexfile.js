const path = require('path');

const BASE_PATH = path.join(__dirname, 'src', 'server', 'db');

module.exports = {
  test: {
    client: 'pg',
    connection: 'postgres://ci_admin_intranet:AdminDB2019children@ciintranetdb.csarmmyervzt.us-east-1.rds.amazonaws.com/koa_api_angel',
    migrations: {
      directory: path.join(BASE_PATH, 'migrations')
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds')
    }
  },
  development: {
    client: 'pg',
    connection: 'postgres://ci_admin_intranet:AdminDB2019children@ciintranetdb.csarmmyervzt.us-east-1.rds.amazonaws.com/koa_api_angel',
    migrations: {
      directory: path.join(BASE_PATH, 'migrations')
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds')
    }
  }
};
